from django.shortcuts import render
from django.http import HttpResponse, HttpResponseBadRequest


def index(request):
	return render(request, "index.html")


def solver(request):
	if request.method == 'POST':
		if 'captchaImage' in request.FILES:
			captchaImage = request.FILES['captchaImage']
			captchaResult = solve(captchaImage)
			return HttpResponse(captchaResult)
		else:
			return HttpResponseBadRequest()
	elif request.method == 'GET':
		return render(request, "solver.html")
	else:
		return HttpResponseBadRequest()


def solve(captchaImage):
	return "I don't solve anything yet"
